# frozen_string_literal: true

module JenkinsfileRunner
  module Commands
    class Init
      class Configuration < ::JenkinsfileRunner::BaseConfiguration
        config_attributes %i[
          jfr_version
          jenkins_home
          jenkins_war
          jenkins_plugins
          agent_type
          build_output
        ]

        def validate
          @errors = []
          @errors.concat(validate_presence(all_attributes - %i[jenkins_plugins]))
          @errors.concat(validate_paths(%i[jenkins_home jenkins_war jenkins_plugins]))
        end

        def docker_agent?
          :docker == agent_type
        end

        private

        def parser
          @parser ||= OptionParser.new do |option|
            option.banner = 'Usage: init [options]'

            option.on '--jfr-version 1.0-beta-11', 'Jenkinsfile Runner version'
            option.on '--jenkins-home /var/jenkins_home'
            option.on '--jenkins-war /usr/share/jenkins/jenkins.war'
            option.on '--jenkins-plugins /usr/share/jenkins/ref/plugins.txt'
            option.on '--agent-type [shell|docker]', [:shell, :docker]
            option.on '--build-output /path/to/tmp/work/dir'

            option.on('-h', '--help', 'Prints this help') do
              puts option
              exit
            end
          end
        end
      end
    end
  end
end
